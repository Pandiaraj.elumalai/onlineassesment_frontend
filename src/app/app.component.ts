import { Component } from '@angular/core';
import { AssesmentService } from './assesment.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  taskForm: FormGroup;
  employeeFile: File;
  questionFile: File;
  arrayBuffer: any;
  employeeFilename = "Choose file";
  questionFilename = "Choose file";
  questionJson = "";
  employeeJson = "";
  title = 'OnlineAssesment';
  constructor(private _assesmetservice: AssesmentService, private fb: FormBuilder) {

  }

  ngOnInit(): void {
    this.taskForm = this.fb.group({
      testType: ['', [Validators.required]],
      date: ['', []],
      duration: ['', [Validators.required]],
      toalQuestion: ['', [Validators.required]],
      totalMark: ['', [Validators.required]],
      totalPassMark: ['', [Validators.required]],
      intsruction: ['', [Validators.required]]

    });
  }

  incomingEmployeeFile(event) {
    this.employeeFile = event.target.files[0];
    this.employeeFilename = this.employeeFile.name;

  }

  incomingQuestionfile(event) {
    this.questionFile = event.target.files[0];
    this.questionFilename = this.questionFile.name;

  }

  onSubmit() {
    console.log(this.taskForm.value);
    console.log(this.questionJson);
    console.log(this.employeeJson);
    let sData = {
      QuestionJsonData: this.questionJson,
      EmployeeJsonData: this.employeeJson,
      TypeId:this.taskForm.controls['testType'].value,
      Date:'',
      Duration:this.taskForm.controls['duration'].value,
      TotalQuestion:this.taskForm.controls['toalQuestion'].value,
      TotalMarks:this.taskForm.controls['totalMark'].value,
      PassMark:this.taskForm.controls['totalPassMark'].value
    };
    this._assesmetservice.saveEmployee(sData).subscribe(res => {
          if (res) {
            console.log('success')
          }
        });

  }

  upLoadEmployee() {

    let vvsfilename = this.employeeFilename;
    // this.employeeFilename = 'BROWSE';
    let worksheet;
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      let data = new Uint8Array(this.arrayBuffer);
      let arr = new Array();
      for (var i = 0; i != data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      let bstr = arr.join("");
      let workbook = XLSX.read(bstr, { type: "binary" });
      let first_sheet_name = workbook.SheetNames[0];
      worksheet = workbook.Sheets[first_sheet_name];
      // worksheet = XLSX.utils.sheet_add_aoa(worksheet,[["ProductId", "ProductName"] ], {origin:0});
      this.employeeJson = JSON.stringify(XLSX.utils.sheet_to_json(worksheet, { raw: false }));

      //questionJson = JSON.stringify(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      // if (questionJson) {
      //   this._assesmetservice.saveEmployee(questionJson, "ng").subscribe(res => {
      //     if (res) {
      //       console.log('success')
      //     }
      //   });
      // }

    }
    fileReader.readAsArrayBuffer(this.employeeFile);


  }

  upLoadQuestion() {

    let vvsfilename = this.questionFilename;
    // this.filename = 'BROWSE';
    let worksheet;
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      let data = new Uint8Array(this.arrayBuffer);
      let arr = new Array();
      for (var i = 0; i != data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      let bstr = arr.join("");
      let workbook = XLSX.read(bstr, { type: "binary" });
      let first_sheet_name = workbook.SheetNames[0];
      worksheet = workbook.Sheets[first_sheet_name];
      // worksheet = XLSX.utils.sheet_add_aoa(worksheet,[["ProductId", "ProductName"] ], {origin:0});
      this.questionJson = JSON.stringify(XLSX.utils.sheet_to_json(worksheet, { raw: false }));

    }
    fileReader.readAsArrayBuffer(this.questionFile);

  }

}
