import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
// import {ErrorObservable} from 'rxjs/Observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AssesmentService {
  url = 'https://localhost:44343/api/Assesment';
  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  constructor(private http: HttpClient) { }

  getEmp(): Observable<any> {

    return this.http.get(this.url, this.httpOptions);
  }

  saveEmployee(aData: any): Observable<any> {
    return this.http.post<any>(this.url,
      aData, this.httpOptions);
  }

  // static handleError(errorres: HttpErrorResponse) {
  //   if (errorres.error instanceof ErrorEvent) {
  //     console.log(errorres.error.message);
  //   } else {
  //     console.log(errorres);
  //   }
  //   return new ErrorObservable('asdsd');
  // }
}
